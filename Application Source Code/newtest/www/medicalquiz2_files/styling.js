(function($) {
    $(document).ready(function() {

        /* Insert div to enclose table if the table applies hightlightBox */
        $('table').each(function() {
            if ($(this).attr('class') == 'highlightBox') {
                $(this).wrap('<div id="divHighLight" class="highlightBox">');
                $(this).removeClass("highlightBox");
            }
        });


        // general faq style, expand n retract effect 
        // get target from expandBody then expand / retract it when click

        $('.expandHead').each(function() {
            var target = $('#' + $(this).attr('expandBody'));
            target.css('display', 'none');
        });

        $('.expandHead').click(function() {
            // look specific answer then display
            var target = $('#' + $(this).attr('expandBody'));

            if (target.css('display') == 'none') {
                target.show(500);
            }
            else {
                target.hide(500);
            }
        });
		
		// new method, expand n retract effect 
		$('table.expandableTable tr:first-child td:first-child').each(function() {
			if($(this).children('blockquote').length > 0) {
				$(this).children('blockquote:first-child').addClass('head');
			}
			else {
				$(this).addClass('head');
			}
			
			$(this)
				.click(function(){
					$(this).parent().siblings().each(function() {
						if($(this).css('display') == 'none'){
							$(this).show(250);
						}else{
							$(this).hide(250);
						}
					});
				})
				.parent().siblings().hide();
		});

    });

})(jQuery);