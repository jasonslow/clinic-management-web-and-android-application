(function($) {

    var MenuPages = new Array();

    MenuPages['ah_listquestion.aspx'] = 'ahteam/ah_listquestionbycategory.aspx';
    MenuPages['ah_listquestionbycategory.aspx'] = 'ahteam/ah_listquestionbycategory.aspx';
    MenuPages['ahteam/ah_listquestion.aspx'] = 'ahteam/ah_listquestionbycategory.aspx';
    MenuPages['ahteam/ah_listquestionbycategory.aspx'] = 'ahteam/ah_listquestionbycategory.aspx';
    MenuPages['ahteam'] = 'ahteam/ah_listquestionbycategory.aspx';
    MenuPages['ah_submitquestion.aspx'] = 'ahteam/ah_submitquestion.aspx';
    MenuPages['ah_submitquestion2.aspx'] = 'ahteam/ah_submitquestion.aspx';
    MenuPages['announcements.aspx'] = 'none';
    MenuPages['appointments.aspx'] = 'enquiries.aspx';
    MenuPages['appointments2.aspx'] = 'enquiries.aspx';
    MenuPages['changeappointment.aspx'] = 'enquiries.aspx';
    MenuPages['changeappointment2.aspx'] = 'enquiries.aspx';
    MenuPages['Default.aspx'] = '';
    MenuPages['enquiries.aspx'] = 'enquiries.aspx';    
    MenuPages['event.aspx'] = 'none';
    MenuPages['events.aspx'] = 'none';
    MenuPages['faq_list.aspx'] = 'enquiries.aspx';
    MenuPages['healthquizzes.aspx'] = 'none';
    MenuPages['healthquizzes_list.aspx'] = 'none';
    MenuPages['joinus.aspx'] = 'none';
    MenuPages['knowyourmedication.aspx'] = 'knowyourmedication.aspx';
    MenuPages['makeappointment.aspx'] = 'enquires.aspx';
    MenuPages['makeappointment2.aspx'] = 'enquires.aspx';
    MenuPages['media.aspx'] = 'media_list.aspx';
    MenuPages['newsletter_list.aspx'] = 'none';
    MenuPages['search.aspx'] = 'none';
    MenuPages['sitemap.aspx'] = 'none';
    MenuPages['smile.aspx'] = 'none';
    MenuPages['smile_list.aspx'] = 'none';
    MenuPages['submission.aspx'] = 'none';
    MenuPages['tender.aspx'] = 'none';
    MenuPages['tender_list.aspx'] = 'none';
    MenuPages['training.aspx'] = 'none';
    MenuPages['updateparticulars.aspx'] = 'enquiries.aspx';
    MenuPages['find_a_polyclinic_near_you'] = 'Find_A_Polyclinic_Near_You/';
    MenuPages['our_services'] = 'Our_Services/';
    MenuPages['be_a_volunteer'] = 'Be_A_Volunteer/';
    MenuPages['about_us'] = 'About_Us/';
    MenuPages['disclaimer'] = 'none';
    MenuPages['health_quizzes'] = 'none';
    MenuPages['our_care_team'] = 'Our_Care_Team/';
	MenuPages['primary_care_academy.aspx'] = 'primary_care_academy.aspx';

    $(document).ready(function() {
        //* Default to home.
        var menuName = '';

        //* Get the current page and find the menu based on it.
        var fileName = location.pathname.substring(1);
		if(fileName.indexOf('/') != -1){
			fileName = fileName.substring(0, fileName.indexOf('/'));
		}
        fileName = fileName.toLowerCase();

        //* Find the menu to map to.
        if (MenuPages[fileName]) {
            menuName = MenuPages[fileName];
        }

        //* Update the menu item class to show selected.
        $("#mainMenu a[href='/" + menuName + "']")
            .addClass('ekmenu_link_selected')
            .removeClass('ekmenu_link')
            .removeClass('ekmenu_submenu_btnlink');

        $("#mainMenu ul li ul li a").removeClass('ekmenu_link_selected');

        // Create a div in the home menu 
    });

})(jQuery);