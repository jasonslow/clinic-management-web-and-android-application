
	function coporatevistor() {

        jQuery('#coporatevisitor').dialog({
            autoOpen: false,
            modal: true,
			draggable: false,
			resizable: false,
            width: 300,
            height: 200,
            show: "slide"
        });

        // ajax setup.
        jQuery.ajaxSetup({
            xhr: function () {
                //return new window.XMLHttpRequest();
                try {
                    if (window.ActiveXObject)
                        return new window.ActiveXObject("Microsoft.XMLHTTP");
                } catch (e) { }

                return new window.XMLHttpRequest();
            }
        });

        var url = window.location.protocol + "//" + window.location.host + "/coporatevisitors.aspx";
        $.get(url, function (result) {
            $("#coporatevisitor").html(result);

        });

        jQuery('#coporatevisitor').dialog('open');
    }


    function loadData() {

        //ajax fix for IE.
        if ($.browser.msie) {
            jQuery.ajaxSetup({
                xhr: function () {
                    //return new window.XMLHttpRequest();
                    try {
                        if (window.ActiveXObject)
                            return new window.ActiveXObject("Microsoft.XMLHTTP");
                    } catch (e) { }

                    return new window.XMLHttpRequest();
                }
            });
        }

        // load faq
        $.ajax({
            type: "GET",
            url: "/coporatevisitors.aspx",
            success: function (result) {
                $("#coporatevisitor").html(result);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.responseText);
            }
        });

    }
        


